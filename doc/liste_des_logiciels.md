# Liste des logiciels

## Base

Ces logiciels sont utiles à toutes les filières.

* gimp
* vlc
* libreoffice
* latex (texlive + lyx + pandoc + texstudio)
* imagemagick
* inkscape
* emacs
* ffmpeg
* firefox
* dia
* transfig
* xfig
* xcrysden

## Électronique

* arduino
* fritzing

## Développement

* build-essential (gcc, g++, make etc.)
* git
* mercurial (avec hg-git et hg-evolve)
* meld
* nano
* eclipse
* gdb
* cmake
* gfortran
* minicom
* python3
* python3-venv
* pytorch
* libarmadillo-dev
* libboost
* libfftw3-3
* anaconda
* vscode

## Géosciences

* google-earth
* grass
* saga
* otb-bin
* parflow
* seiscomp
* seismic-unix
* qgis

## Calcul scientifique

 * r-base
 * octave (+addons)
 * scilab
 * root-system
 * freefem++
 * wxmaxima
 * xcas
 * gnuplot
 * yade

## CAO

 * gmsh
 * freecad

 ## MAO

 * ardour
 * puredata

 