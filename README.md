# Installation de logiciels Linux pour PhITEM

*Statut* : stable

*Objectifs* :
  * permettre aux enseignants ou étudiants d'installer simplement un logiciel avec la recette utilisée à PhITEM ;
  * en interne, permettre l'installation de groupes de logiciels pour le provisionning Ansible (pour la création des modèles LTSP).

*Compatibilité* :
  * Ubuntu 20.04
  * Ubuntu 22.04


## Installation

Télécharger ou cloner ce dépôt. Placez-vous ensuite dans le dossier contenant ce README.

ex.

```bash
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/phitem/phitem-linux-logiciels
cd phitem-linux-logiciels
```

## Utilisation

### Installer un logiciel

```bash
bash ph-install <logiciel>
```

### Installer un bundle de logiciels localement

```bash
ansible-playbook <bundle.yml>
```

### Installer tous les logiciels localement

```bash
ansible-playbook phitem-linux.yml
```

### Mettre à jour ph-install

Si des correctifs ou des nouvelles recettes d'installation ont été ajoutées, vous pouvez mettre à jour _ph-install_ avec la dernière version :

```bash
cd phitem-linux-logiciels
git pull
```

### Utiliser en tant que rôle Ansible

(nécessite une configuration fonctionnelle de Ansible et un accès SSH au poste à configurer)

FIXME


## Notes

Un grand nombre de logiciels sont facilement installables depuis les dépôts officiels Ubuntu, mais d'autres sont plus complexes à installer. C'est une des raisons de la mise en place de ce dépôt.

## Développement

### Tests

  - installation individuelle de chaque logiciel
  - installation de tous les logiciels via `ansible-playbook phitem-linux.yml`
